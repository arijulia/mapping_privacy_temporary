package com.arijulia.map.services.impl;

/**
 * This class start an wifi p2p access point and then register a local service.
 * Created by yedtoss on 2/6/16.
 */

import com.arijulia.map.services.Constants;

import android.net.wifi.WifiManager;
import android.util.Log;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;

import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.support.v4.content.LocalBroadcastManager;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static android.net.wifi.p2p.WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION;

import static android.net.wifi.p2p.WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION;


/**
 *
 */
public class WifiAccessPoint implements WifiP2pManager.ConnectionInfoListener,
        WifiP2pManager.ChannelListener,WifiP2pManager.GroupInfoListener{


    WifiAccessPoint that = this;

    // The context
    Context context;

    // The wifi p2p manager
    private WifiP2pManager p2pManager;

    // The wifi p2p channel
    private WifiP2pManager.Channel p2pChannel;

    // The network name, passphrase and group owner address
    String mNetworkName = "";
    String mPassphrase = "";
    String mInetAddress = "";

    // TAG for loggging
    private static final String TAG = "WifiP2pAccessPoint";

    // The group creating broacast reciever
    private BroadcastReceiver receiver;

    // The intent filter for the broacast receiver above
    private IntentFilter filter;

    int[] freePorts;


    // Constructor for the class
    public WifiAccessPoint(Context Context, int[] freePorts) {
        this.context = Context;
        this.freePorts = freePorts;
        Log.d(TAG, "FreePorts are " + freePorts[0] + ":" + freePorts[1]);

    }

    // Start the wifi p2p soft access point
    public void start() {

        // Get the wifi p2p manager
        p2pManager = (WifiP2pManager) this.context.getSystemService(this.context.WIFI_P2P_SERVICE);

        if (p2pManager == null) {
            //print("The WIFI p2p is not a supported system service");
            Log.d(TAG, "The WIFI p2p is not a supported system service");
        } else {

            Log.d(TAG, "Initializing p2p");

            // Initialize the wifi p2p manager and get the corresponding channel
            p2pChannel = p2pManager.initialize(this.context, this.context.getMainLooper(), this);

            // Create the receiver that will handle group creation broacast messages
            receiver = new AccessPointReceiver();
            filter = new IntentFilter();
            // We want to handle state changed action to know if wifi p2p is enabled or not
            filter.addAction(WIFI_P2P_STATE_CHANGED_ACTION);

            // We want to handle connection changed action to know if group is created
            filter.addAction(WIFI_P2P_CONNECTION_CHANGED_ACTION);
            this.context.registerReceiver(receiver, filter);

            // Here we are creating the group that will then create the soft access point
            // Once the creating is finished a WIFI_P2P_CONNECTION_CHANGED_ACTION will be broadcast
            // So we should listen for it
            p2pManager.createGroup(p2pChannel,new WifiP2pManager.ActionListener() {
                public void onSuccess() {
                    Log.d(TAG, "Creating Local Group ");
                }

                public void onFailure(int reason) {
                    Log.d(TAG, "Local Group failed, error code " + reason);
                    if(reason == WifiP2pManager.P2P_UNSUPPORTED){
                        Log.d(TAG, "Local Group failed. reason: the operation failed" +
                                " because p2p is unsupported on the device");
                    } else if(reason == WifiP2pManager.BUSY){
                        Log.d(TAG, "Local Group failed. reason: the operation failed due" +
                                " to an internal error");

                    } else if(reason == WifiP2pManager.ERROR){
                        Log.d(TAG, "Local Group failed. reason: the operation failed because" +
                                " the framework is busy and unable to service the request");
                    }
                }
            });
        }
    }


    // This method will stop the wifi p2p access point by unregistering the broacast
    // receiver, stopping the started local services and removing the group created
    public void stop() {
        Log.d(TAG, "Stopping p2p access point by removing group and stopping local service");
        this.context.unregisterReceiver(receiver);
        stopLocalServices();
        removeGroup();
    }

    // This will the current p2p group
    public void removeGroup() {
        p2pManager.removeGroup(p2pChannel,new WifiP2pManager.ActionListener() {
            public void onSuccess() {
                Log.d(TAG, "Cleared Local Group ");
            }

            public void onFailure(int reason) {
                Log.d(TAG, "Clearing Local Group failed, error code " + reason);
            }
        });
    }

    @Override
    public void onChannelDisconnected() {
        // see how we could avoid looping
        //     p2p = (WifiP2pManager) this.context.getSystemService(this.context.WIFI_P2P_SERVICE);
        //     channel = p2p.initialize(this.context, this.context.getMainLooper(), this);
    }

    // methods needed for the group info listener
    @Override
    public void onGroupInfoAvailable(WifiP2pGroup group) {
        try {

            // Printing the list of clients to this group.
            // But we are not expecting to have any right now??
            /*Collection<WifiP2pDevice>  devlist = group.getClientList();

            int numm = 0;
            for (WifiP2pDevice peer : group.getClientList()) {
                numm++;
               //print("Client " + numm + " : "  + peer.deviceName + " " + peer.deviceAddress);
            }*/

            // Is this if needed?
            if(mNetworkName.equals(group.getNetworkName()) && mPassphrase.equals(group.getPassphrase())){
                Log.d(TAG, "Already have local service for " + mNetworkName + " ," + mPassphrase);
            }else {

                mNetworkName = group.getNetworkName();
                mPassphrase = group.getPassphrase();
                startAccessPointLocalService(group.getNetworkName(), group.getPassphrase(), mInetAddress);
            }
        } catch(Exception e) {
            Log.e(TAG, "onGroupInfoAvailable, error: " + e.toString());
        }
    }

    // This will register the local service
    private void startAccessPointLocalService(String ssid, String passphrase, String mInetAddress) {

        Log.d(TAG, "Enterting startAccessPointLocalService with ssid: " + ssid + "; passphrase: "
                + passphrase + ";address:"+ mInetAddress);

        // This will contain info needed by other peers
        Map<String, String> record = new HashMap<String, String>();
        //record.put("v", "y"); // visibility
        record.put(Constants.SSID_KEY, ssid);
        record.put(Constants.PASSPHRASE_KEY, passphrase);
        record.put(Constants.ADDRESS_KEY, mInetAddress); // group owner address
        record.put(Constants.TCP_PORT_KEY, "" + freePorts[0]);
        record.put(Constants.UDP_PORT_KEY, "" + freePorts[1]);

        // If other infos are needed pass it in the map

        // Should we use another instance name?
        //String instanceName = "NI:" + ssid + ":" + passphrase + ":" + mInetAddress + ":"
        //        + freePorts[0] + ":" + freePorts[1];
        String instanceName = "_test";


        // Service information
        WifiP2pDnsSdServiceInfo service = WifiP2pDnsSdServiceInfo.newInstance(instanceName, Constants.SERVICE_TYPE, record);

        //Log.d(TAG, Add local service :" + instance);
        // Add the local service, sending the service info, network channel,
        // and listener that will be used to indicate success or failure of
        // the request.
        p2pManager.addLocalService(p2pChannel, service, new WifiP2pManager.ActionListener() {
            public void onSuccess() {
                Log.d(TAG, "Added local service");
            }

            public void onFailure(int reason) {
                Log.d(TAG, "Adding local service failed, error code " + reason);
            }
        });
    }

    // Stop the local service
    private void stopLocalServices() {
        mNetworkName = "";
        mPassphrase = "";

        p2pManager.clearLocalServices(p2pChannel, new WifiP2pManager.ActionListener() {
            public void onSuccess() {
                Log.d(TAG, "Cleared local services");
            }

            public void onFailure(int reason) {
                Log.d(TAG, "Clearing local services failed, error code " + reason);
            }
        });
    }

    // This is the method needed for the connection listener
    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo info) {
        try {
            // Do we really need groupFormed here???? Or should it be removed
            if (info.groupFormed && info.isGroupOwner) {
                /*if(broadcaster != null) {
                    mInetAddress = info.groupOwnerAddress.getHostAddress();
                    Intent intent = new Intent(DSS_WIFIAP_SERVERADDRESS);
                    intent.putExtra(DSS_WIFIAP_INETADDRESS, (Serializable)info.groupOwnerAddress);
                    broadcaster.sendBroadcast(intent);
                }*/
                Log.d(TAG, "Goup owner detected");
                mInetAddress = info.groupOwnerAddress.getHostAddress();
                // We request now the group information
                p2pManager.requestGroupInfo(p2pChannel,this);
            } else {
                // We are not expected to do anything here as we are not planning to
                // connect clients using standard p2p connection
                Log.d(TAG, "we are client !! group owner address is: " + info.groupOwnerAddress.getHostAddress());
            }
        } catch(Exception e) {
            Log.d(TAG, "onConnectionInfoAvailable, error: " + e.toString());
        }
    }


    private class AccessPointReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
                // Determine if Wifi Direct mode is enabled or not, alert
                // the Activity.
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                    //activity.setIsWifiP2pEnabled(true);
                    Log.d(TAG, "Wifi p2p is enabled");
                } else {
                    //activity.setIsWifiP2pEnabled(false);
                    Log.d(TAG, "Wifi p2p is disabled");
                }

            }  else if (WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
                NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                if (networkInfo.isConnected()) {
                    Log.d(TAG, "We are connected, will check info now");
                    p2pManager.requestConnectionInfo(p2pChannel, that);
                } else{
                    Log.d(TAG, "We are disconnected");
                }
            }
        }
    }
}
