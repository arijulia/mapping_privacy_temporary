package com.arijulia.map.services.impl;

import android.util.Log;
import android.os.AsyncTask;

import com.arijulia.map.services.AnswerRequest;
import com.arijulia.map.services.AnswerResponse;
import com.arijulia.map.services.Constants;
import com.arijulia.map.services.LocationRequest;
import com.arijulia.map.services.LocationResponse;
import com.arijulia.map.services.utils.Helper;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import java.io.IOException;

/**
 * Created by yedtoss on 2/6/16.
 */
public class SocketClient {

    public Client getClient() {
        return client;
    }

    private Client client;
    private static final String TAG = "SocketClient";

    private int tcpPort;

    private int udpPort;


    public SocketClient(String tcpPort, String udpPort){

        this.tcpPort = Helper.toInt(tcpPort);
        this.udpPort = Helper.toInt(udpPort);
    }

    public void start(final String ip, final BaseCallBack callBack){
        Log.d(TAG, "Connecting client to ip ... "+ ip);

        client = new Client();
        SocketCommnunication.register(client);
        client.start();
        // Handle response from server
        client.addListener(new Listener() {
            public void received(Connection connection, Object object) {
                if (object instanceof LocationResponse) {
                    LocationResponse response = (LocationResponse) object;
                    Log.d(TAG, "Received message " + response.getMessage() + " from server");

                    if(response.isChosen()){

                        Log.d(TAG, "Client sending answer request");
                        final AnswerRequest request = new AnswerRequest();
                        request.setResponse("There is Cafe Citoyen at Lille View Palais");

                        new AsyncTask<Void, Void, Void>(){

                            @Override
                            protected Void doInBackground( final Void ... params ) {
                                // something you know that will take a few seconds
                                client.sendTCP(request);
                                return null;
                            }

                            @Override
                            protected void onPostExecute( final Void result ) {
                                //
                            }

                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

                    }
                } else if(object instanceof AnswerResponse){

                    Log.d(TAG, "Received answer response from server ");

                }
            }
        });

        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground( final Void ... params ) {
                // something you know that will take a few seconds
                try{
                    client.setTimeout(0);
                    client.connect(Constants.CONNECTION_TIMEOUT, ip,
                            tcpPort, udpPort);
                    Log.d(TAG, "Client successfully connected");
                } catch (IOException e){
                    Log.e(TAG, "Error connecting to the socket server: ", e);
                }

                return null;
            }

            @Override
            protected void onPostExecute( final Void result ) {
                callBack.run();
            }

        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);





    }

    public void sendLocationRequest(final LocationRequest request){
        Log.d(TAG, "Client sending request");

        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground( final Void ... params ) {
                // something you know that will take a few seconds
                client.sendTCP(request);
                return null;
            }

            @Override
            protected void onPostExecute( final Void result ) {
                //
            }

        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

    }
}
