package com.arijulia.map.services;

/**
 * Created by yedtoss on 2/6/16.
 */
public enum WifiAccessPointState {
    NONE,
    STOPPED,
    STARTED,
    GROUP_STOPPED,
    LOCAL_SERVICE_STOPPED,
    GROUP_STARTED,
    LOCAL_SERVICE_STARTED,

}
