package com.arijulia.map.services.impl;

/**
 * This class perform local service discovery through p2p
 * The request made and then we listen for service discovery.
 * The request and discovery are only initiated if we detected peers.
 * Created by yedtoss on 2/6/16.
 */
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import  android.net.wifi.p2p.WifiP2pManager.DnsSdTxtRecordListener;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.WindowManager;

import com.arijulia.map.HomeActivity;
import com.arijulia.map.services.Constants;
import com.arijulia.map.services.LocationRequest;
import com.arijulia.map.services.ServiceState;

import java.util.Map;

import static android.net.wifi.p2p.WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION;
import static android.net.wifi.p2p.WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION;
import static android.net.wifi.p2p.WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION;
import static android.net.wifi.p2p.WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION;


public class WifiP2pServiceDiscovery  implements WifiP2pManager.ChannelListener{

    // TAG for loggging
    private static final String TAG = "WifiP2pServiceDiscovery";

    WifiP2pServiceDiscovery that = this;
    Context context;

    private BroadcastReceiver receiver;
    private IntentFilter filter;

    private WifiP2pManager p2pManager;
    private WifiP2pManager.Channel channel;
    private WifiP2pManager.DnsSdServiceResponseListener serviceListener;
    private WifiP2pManager.PeerListListener peerListListener;


    ServiceState myServiceState = ServiceState.NONE;
    Map record;


    public WifiP2pServiceDiscovery(Context Context) {
        this.context = Context;
    }


    public void start() {

        p2pManager = (WifiP2pManager) this.context.getSystemService(this.context.WIFI_P2P_SERVICE);
        if (p2pManager == null) {
            Log.d(TAG, "The WIFI p2pManager is not a supported system service");
        }else {

            channel = p2pManager.initialize(this.context, this.context.getMainLooper(), this);

            receiver = new ServiceSearcherReceiver();
            filter = new IntentFilter();
            filter.addAction(WIFI_P2P_STATE_CHANGED_ACTION);
            filter.addAction(WIFI_P2P_CONNECTION_CHANGED_ACTION);
            filter.addAction(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION);
            filter.addAction(WIFI_P2P_PEERS_CHANGED_ACTION);

            this.context.registerReceiver(receiver, filter);

            peerListListener = new WifiP2pManager.PeerListListener() {

                public void onPeersAvailable(WifiP2pDeviceList peers) {

                    int num = 0;
                    for (WifiP2pDevice peer : peers.getDeviceList()) {
                        num++;
                        Log.d(TAG, "\t" + num + ": "  + peer.deviceName + " " + peer.deviceAddress);
                    }

                    if(num > 0){
                        startServiceDiscovery();
                    }else{
                        Log.d(TAG, "No peers detected");
                        startPeerDiscovery();
                    }
                }
            };
            DnsSdTxtRecordListener txtListener = new DnsSdTxtRecordListener() {
                @Override
        /* Callback includes:
         * fullDomain: full domain name: e.g "printer._ipp._tcp.local."
         * record: TXT record dta as a map of key/value pairs.
         * device: The device running the advertised service.
         */
                public void onDnsSdTxtRecordAvailable(
                        String fullDomain, Map record, WifiP2pDevice device) {
                        Log.d(TAG, "DnsSdTxtRecord available -" + record.toString());
                    if(record.containsKey(Constants.PASSPHRASE_KEY) && record.containsKey(Constants.SSID_KEY)){
                        WifiConnection mWifiConnection = new WifiConnection(context,
                                record.get(Constants.SSID_KEY).toString(),
                                record.get(Constants.PASSPHRASE_KEY).toString());
                        //mWifiConnection.SetInetAddress(data[3]);
                        mWifiConnection.findAddress();
                        that.record = record;
                    } else {
                        Log.d(TAG, "Unknow record detected");
                    }

                }
            };

            serviceListener = new WifiP2pManager.DnsSdServiceResponseListener() {

                public void onDnsSdServiceAvailable(String instanceName, String serviceType, WifiP2pDevice device) {

                    if (serviceType.startsWith(Constants.SERVICE_TYPE)) {

                        //instance name has AP information for Client connection
                        Log.d(TAG, "Service of name: " + instanceName + " is available");

                        final Handler handler = new Handler();
                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setTitle("Connection Info");
                        alertDialog.setMessage("Please connect to the network:<"
                                + record.get(Constants.SSID_KEY) + "> with password:<"
                                + record.get(Constants.PASSPHRASE_KEY) + ">. > for info only. This will become automatic after proof-of-concept.  Click only on OK when you are connected to the network.");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                // Connecting to the socket server
                                                final SocketClient client = new SocketClient(
                                                        record.get(Constants.TCP_PORT_KEY).toString(),
                                                        record.get(Constants.UDP_PORT_KEY).toString());
                                                client.start(record.get(Constants.ADDRESS_KEY).toString(), new BaseCallBack() {
                                                    @Override
                                                    public void run() {
                                                        LocationRequest request = new LocationRequest();
                                                        request.setMessage("My gps location is 234:345");
                                                        client.sendLocationRequest(request);
                                                    }
                                                });
                                            }
                                        }, 1000);
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                        alertDialog.show();






//                        String[] data = instanceName.split(":");
//
//                        WifiConnection mWifiConnection = new WifiConnection(context,
//                                data[1], data[2]);
//                        //mWifiConnection.SetInetAddress(data[3]);
//
//
//                        // Connecting to the socket server
//                        final SocketClient client = new SocketClient(data[4], data[5]);
//                        client.start(data[3], new BaseCallBack() {
//                            @Override
//                            public void run() {
//                                LocationRequest request = new LocationRequest();
//                                request.setMessage("My gps location is 234:345");
//                                client.sendLocationRequest(request);
//                            }
//                        });


                    } else {
                        Log.d(TAG, "Not our Service, :" + Constants.SERVICE_TYPE + "!=" + serviceType + ":");
                    }

                    startPeerDiscovery();
                }
            };

            p2pManager.setDnsSdResponseListeners(channel, serviceListener, txtListener);
            startPeerDiscovery();
        }
    }


    public void stop() {
        this.context.unregisterReceiver(receiver);
        stopDiscovery();
        stopPeerDiscovery();
    }

    @Override
    public void onChannelDisconnected() {
        //
    }
    private void startPeerDiscovery() {
        Log.d(TAG, "Entering startPeerDiscovery()");
        p2pManager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
            public void onSuccess() {
                myServiceState = ServiceState.DiscoverPeer;
                Log.d(TAG, "Started peer discovery");
            }

            public void onFailure(int reason) {
                Log.d(TAG, "Starting peer discovery failed, error code " + reason);

                if(Constants.isMockedEnabled){
                    WifiConnection mWifiConnection = new WifiConnection(context,
                            Constants.mockSsid, Constants.mockPassphrase);

                    final Handler handler = new Handler();

                    AlertDialog alertDialog = new AlertDialog.Builder(that.context).create();
                    alertDialog.setTitle("Connection Info");
                    alertDialog.setMessage("Please connect to the network:<"
                            + Constants.mockSsid + "> with password:<"
                            + Constants.mockPassphrase + ">. > for info only. This will become automatic after proof-of-concept.  Click only on OK when you are connected to the network.");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    handler.postDelayed(new Runnable() {

                                        public void run() {
                                            final SocketClient client = new SocketClient("" + Constants.SERVER_TCP_PORT,
                                                    "" + Constants.SERVER_UDP_PORT);
                                            client.start(Constants.mockAddress, new BaseCallBack() {
                                                @Override
                                                public void run() {
                                                    LocationRequest request = new LocationRequest();
                                                    request.setMessage("My gps location is 234:345");
                                                    client.sendLocationRequest(request);
                                                }
                                            });
                                        }

                                    }, 1000);
                                    dialog.dismiss();

                                }
                            });
                    alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                    alertDialog.show();



                }

            }
        });
    }

    private void stopPeerDiscovery() {
        p2pManager.stopPeerDiscovery(channel, new WifiP2pManager.ActionListener() {
            public void onSuccess() {
                Log.d(TAG, "Peer discovery successfully stopped");
            }

            public void onFailure(int reason) {
                Log.d(TAG, "Stopping peer discovery failed, error code " + reason);
            }
        });
    }

    private void startServiceDiscovery() {

        // See http://www.drjukka.com/blog/wordpress/?p=127
        // It seems using service type in the request lead to the record not being passed in txt
        // Only instance name is passed
        // Also great limitation of size in record whereas instance name can be a little longer
        //WifiP2pDnsSdServiceRequest request = WifiP2pDnsSdServiceRequest.newInstance(Constants.SERVICE_TYPE);
        WifiP2pDnsSdServiceRequest request = WifiP2pDnsSdServiceRequest.newInstance();
        final Handler handler = new Handler();
        p2pManager.addServiceRequest(channel, request, new WifiP2pManager.ActionListener() {

            public void onSuccess() {
                Log.d(TAG, "Service discovery request successfully made");
                handler.postDelayed(new Runnable() {
                    //There are supposedly a possible race-condition bug with the service discovery
                    // thus to avoid it, we are delaying the service discovery start here
                    public void run() {
                        p2pManager.discoverServices(channel, new WifiP2pManager.ActionListener() {
                            public void onSuccess() {
                                Log.d(TAG, "Service discovery successfully started");
                                myServiceState = ServiceState.DiscoverService;
                            }

                            public void onFailure(int reason) {
                                Log.d(TAG, "Starting service discovery failed, error code " + reason);
                            }
                        });
                    }
                }, 1000);
            }

            public void onFailure(int reason) {
                Log.d(TAG, "Service discovery request failed, error code " + reason);
                // No point starting service discovery
            }
        });

    }

    private void stopDiscovery() {
        p2pManager.clearServiceRequests(channel, new WifiP2pManager.ActionListener() {
            public void onSuccess() {
                Log.d(TAG, "Cleared service requests");
            }

            public void onFailure(int reason) {
                Log.d(TAG, "Clearing service requests failed, error code " + reason);
            }
        });
    }

    private class ServiceSearcherReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {

                } else {

                }
            }else if(WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
                if(myServiceState != ServiceState.DiscoverService) {
                    p2pManager.requestPeers(channel, peerListListener);
                }
            } else if(WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
                //WifiP2pDevice device = intent.getParcelableExtra(EXTRA_WIFI_P2P_DEVICE);
                //addText("Local device: " + MyP2PHelper.deviceToString(device));
            } else if(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION.equals(action)) {

                int state = intent.getIntExtra(WifiP2pManager.EXTRA_DISCOVERY_STATE, WifiP2pManager.WIFI_P2P_DISCOVERY_STOPPED);
                String persTatu = "Discovery state changed to ";

                if(state == WifiP2pManager.WIFI_P2P_DISCOVERY_STOPPED){
                    persTatu = persTatu + "Stopped.";
                    startPeerDiscovery();
                }else if(state == WifiP2pManager.WIFI_P2P_DISCOVERY_STARTED){
                    persTatu = persTatu + "Started.";
                }else{
                    persTatu = persTatu + "unknown  " + state;
                }
                Log.d(TAG, persTatu);

            } else if (WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
                NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                if (networkInfo.isConnected()) {
                    Log.d(TAG, "Connected");
                    startPeerDiscovery();
                } else{
                    Log.d(TAG, "DIS-Connected");
                    startPeerDiscovery();
                }
            }
        }
    }
}
