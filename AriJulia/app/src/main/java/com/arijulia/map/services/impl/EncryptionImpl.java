package com.arijulia.map.services.impl;

import android.util.Base64;
import android.util.Log;

import com.arijulia.map.services.AESEncryptedState;

import java.security.AlgorithmParameters;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by yedtoss on 2/7/16.
 */
public class EncryptionImpl {

    private static final String TAG = "EncryptionImpl";

    public KeyPair generateKeyPair(){

        Log.d(TAG, "Entering generateKeyPair");

        KeyPair kp = null;

        try{
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            //KeyPairGenerator kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA,
            //        Constants.ANDROID_STORE);
            kpg.initialize(1024);
            kp = kpg.genKeyPair();

        } catch (NoSuchAlgorithmException e){
            Log.e(TAG, "This device does not support rsa encryption ", e);
        } catch (Exception e){
            Log.e(TAG, "internal error ", e);
        }

        return kp;


    }


    public byte[] encodeBase64(byte[] request){
        return  Base64.encode(request, Base64.DEFAULT);
    }

    public byte[] decodeBase64(byte[] request){
        return Base64.decode(request, Base64.DEFAULT);
    }



    public byte[] encryptRSA(byte[] request, Key publicKey){
        Log.d(TAG, "Entering encryptRSA(String request, Key publicKey)");


        byte[] encodedBytes = null;
        try{
            Cipher c = Cipher.getInstance("RSA/ECB/NoPadding");
            c.init(Cipher.ENCRYPT_MODE, publicKey);
            encodedBytes = c.doFinal(request);
        } catch (NoSuchAlgorithmException e){
            Log.e(TAG, "This device does not support rsa encryption " , e);
        } catch (InvalidKeyException e){
            Log.e(TAG, "This key is invalid " , e);
        } catch (Exception e){
            Log.e(TAG, "Internal error " , e);
        }


        return encodedBytes;


    }


    public byte[] decryptRSA(byte[] encodedRequest, Key privateKey) {

        Log.d(TAG, "Entering decryptRSA(byte[] encodedRequest, Key privateKey)");
        byte[] decodedBytes = null;
        try {
            Cipher c = Cipher.getInstance("RSA/ECB/NoPadding");
            c.init(Cipher.DECRYPT_MODE, privateKey);
            decodedBytes = c.doFinal(encodedRequest);
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "This device does not support rsa encryption ", e);
        } catch (InvalidKeyException e) {
            Log.e(TAG, "This key is invalid " , e);
        } catch (Exception e) {
            Log.e(TAG, "Internal error ", e);
        }
        return decodedBytes;
    }


    public Key loadPrivateKey(byte[] data){

        Log.d(TAG, "Entering loadPrivateKey(byte[] data)");
        Key privateKey = null;
        PKCS8EncodedKeySpec keySpec = new  PKCS8EncodedKeySpec(data);
        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            privateKey = kf.generatePrivate(keySpec);
        } catch (Exception e){
            Log.e(TAG, "Internal error " , e);
        }

        return  privateKey;

    }


    public Key loadPublicKey(byte[] data){
        Log.d(TAG, "Entering loadPublicKey(byte[] data)");
        Key publicKey = null;

        try{
            KeyFactory kf = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(data);
            publicKey = kf.generatePublic(keySpec);
        } catch (Exception e){
            Log.e(TAG, "Internal error ", e);
        }

        return publicKey;
    }


    public void test(){

        KeyPair kp = generateKeyPair();
        String request = "Test";
        byte[] bs = encodeBase64(request.getBytes());
        byte[] en = encryptRSA(bs, kp.getPublic());
        byte[] de = decryptRSA(en, kp.getPrivate());
        Log.d(TAG, new String(decodeBase64(de)));

        en = encryptRSA(bs, loadPublicKey(kp.getPublic().getEncoded()));
        de = decryptRSA(en, loadPrivateKey(kp.getPrivate().getEncoded()));
        try{
            Log.d(TAG, "Loads find " + new String(decodeBase64(de)));
        } catch (Exception e){
            Log.e(TAG, "UTF 8 did not work", e);
        }


        // Testing AES + RSA
        request = "Hello here, I want to know restaurant near me";
        // As the key is going to be encrypted using rsa let's mimic it
        SecretKey key = generateRandomAESKey();
        AESEncryptedState state = encryptAES(encodeBase64(request.getBytes()), key);
        // Encode the key to base64
        bs = encodeBase64(key.getEncoded());
        // Encrypt the base 64 encoded key using rsa
        en = encryptRSA(bs, kp.getPublic());
        // Decrypt the base 64 encode key with rsa
        de = decryptRSA(en, kp.getPrivate());
        // Decode the base 64
        de = decodeBase64(de);
        // Get the key
        key = loadSecretKey(de);
        // Now use AES to encrypt the request
        bs = encodeBase64(request.getBytes());
        de = decryptAES(state, key);
        Log.d(TAG, "Original AES base 64 request is "+ new String(bs));
        Log.d(TAG, "decrypted AES base 64 is  " + new String(de));

        Log.d(TAG, "Original request is "+ request);
        Log.d(TAG, "Encrypted + decrypted is " + new String(decodeBase64(de)));
    }


    public SecretKey generateRandomAESKey(){

        SecretKey secretKey = null;

        try{
            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(256); // for example
            secretKey = keyGen.generateKey();
        } catch (Exception e){
            Log.e(TAG, "Internal Error ", e);
        }

        return secretKey;
    }


    public AESEncryptedState encryptAES(byte[] request, SecretKey key){

        Log.d(TAG, "Entering encryptAES(byte[] request, SecretKey key)");


        byte[] encodedBytes = null;
        byte[] iv = null;
        try{
            Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            c.init(Cipher.ENCRYPT_MODE, key);
            AlgorithmParameters parameters = c.getParameters();
            iv = parameters.getParameterSpec(IvParameterSpec.class).getIV();
            encodedBytes = c.doFinal(request);
        } catch (NoSuchAlgorithmException e){
            Log.e(TAG, "This device does not support AES encryption ", e);
        } catch (InvalidKeyException e){
            Log.e(TAG, "This key is invalid " , e);
        } catch (Exception e){
            Log.e(TAG, "Internal error ", e);
        }
        return new AESEncryptedState(encodedBytes, iv);

    }

    public byte[] decryptAES(AESEncryptedState state, SecretKey key) {

        Log.d(TAG, "Entering decryptAES(byte[] encodedRequest, SecretKey key)");
        byte[] decodedBytes = null;
        try {
            Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            c.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(state.getIv()));
            decodedBytes = c.doFinal(state.getEncrypted());
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "This device does not support AES encryption ", e);
        } catch (InvalidKeyException e) {
            Log.e(TAG, "This key is invalid " , e);
        } catch (Exception e) {
            Log.e(TAG, "Internal error ", e);
        }
        return decodedBytes;
    }


    public SecretKey loadSecretKey(byte[] key){
        return new SecretKeySpec(key, "AES");
    }


}
