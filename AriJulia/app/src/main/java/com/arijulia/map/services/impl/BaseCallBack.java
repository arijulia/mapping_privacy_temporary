package com.arijulia.map.services.impl;

/**
 * Created by yedtoss on 2/7/16.
 */
public abstract class BaseCallBack {

    public abstract void run();
}
