package com.arijulia.map.services;

/**
 * Created by yedtoss on 2/6/16.
 */
public class LocationRequest {

    public int getLatitude() {
        return latitude;
    }

    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    private int latitude;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getLongitude() {

        return longitude;
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    private int longitude;
    private String message;

    private boolean available;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public boolean isChosen() {
        return chosen;
    }

    public void setChosen(boolean chosen) {
        this.chosen = chosen;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    private String request;

    private String response;

    private boolean chosen;




}
