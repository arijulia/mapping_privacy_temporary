package com.arijulia.map.services;

/**
 * Created by yedtoss on 2/7/16.
 */
public class AESEncryptedState {


    public AESEncryptedState(byte[] encrypted, byte[] iv){
        this.encrypted = encrypted;
        this.iv = iv;
    }


    public byte[] getEncrypted() {
        return encrypted;
    }

    public void setEncrypted(byte[] encrypted) {
        this.encrypted = encrypted;
    }

    private byte[] encrypted;

    public byte[] getIv() {
        return iv;
    }

    public void setIv(byte[] iv) {
        this.iv = iv;
    }

    private byte[] iv;
}
