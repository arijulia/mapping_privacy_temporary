package com.arijulia.map.services;

/**
 * Created by yedtoss on 2/6/16.
 */

public enum ServiceState{
    NONE,
    DiscoverPeer,
    DiscoverService
}
