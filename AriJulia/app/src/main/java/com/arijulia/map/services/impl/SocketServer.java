package com.arijulia.map.services.impl;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.arijulia.map.services.AnswerRequest;
import com.arijulia.map.services.AnswerResponse;
import com.arijulia.map.services.LocationRequest;
import com.arijulia.map.services.LocationResponse;
import com.arijulia.map.services.utils.Helper;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by yedtoss on 2/6/16.
 */
public class SocketServer {

    private static final String TAG = "SocketServer";

    public Server getServer() {
        return server;
    }

    private Server server;

    public int[] getFree_ports() {
        return free_ports;
    }

    private int[] free_ports;

    public AtomicBoolean getRequestPending() {
        return requestPending;
    }

    public void setRequestPending(AtomicBoolean requestPending) {
        this.requestPending = requestPending;
    }

    public AtomicBoolean getPeerChosen() {
        return peerChosen;
    }

    public void setPeerChosen(AtomicBoolean peerChosen) {
        this.peerChosen = peerChosen;
    }

    private AtomicBoolean requestPending;

    private AtomicBoolean peerChosen;

    public String getRequestText() {
        return requestText;
    }

    public void setRequestText(String requestText) {
        this.requestText = requestText;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    private String requestText;

    private String responseText;
    Context context;

    private SocketServer that = this;

    public  SocketServer(Context context){

        requestPending = new AtomicBoolean(false);
        peerChosen = new AtomicBoolean(false);

        //requestPending.set(false);
        //peerChosen.set(false);
        this.context = context;
    }


    public void start(){
        Log.d(TAG, "Starting server ...");
        server = new Server();
        SocketCommnunication.register(server);
        server.start();
        try{
            free_ports = Helper.getFreePorts(2);
            //server.bind(Constants.SERVER_TCP_PORT, Constants.SERVER_UDP_PORT);
            server.bind(free_ports[0], free_ports[1]);
            Log.d(TAG, "Server successfully Started ... at port s "+ free_ports[0] + ":" + free_ports[1]);
        } catch (IOException e){
            Log.e(TAG, "Error when creating socket server: ", e);
        }



        // Adding server listener
        server.addListener(new Listener() {
            public void received (Connection connection, Object object) {
                if (object instanceof LocationRequest) {

                    synchronized (peerChosen){

                        if(requestPending.get()){

                            if(!peerChosen.get()){


                                peerChosen.set(true);

                                LocationRequest request = (LocationRequest)object;
                                Log.d(TAG, "Received location requestText and chosen peer: " + request.getMessage());

                                LocationResponse response = new LocationResponse();
                                response.setMessage("Thanks for accepting to forward my requestText");
                                response.setChosen(true);
                                response.setRequest(requestText);
                                Log.d(TAG, "responseText:Thanks sent to client");
                                connection.sendTCP(response);

                            } else{

                                LocationRequest request = (LocationRequest)object;
                                Log.d(TAG, "Received location requestText and peer not chosen: " + request.getMessage());

                                LocationResponse response = new LocationResponse();
                                response.setMessage("Thanks for accepting to forward my requestText but I already selected someone else");
                                response.setChosen(false);
                                //response.setRequest(requestText);
                                Log.d(TAG, "responseText:Thanks sent to client");
                                connection.sendTCP(response);



                            }
                        } else {

                            LocationRequest request = (LocationRequest)object;
                            Log.d(TAG, "Received location requestText: " + request.getMessage());

                            LocationResponse response = new LocationResponse();
                            response.setMessage("Thanks");
                            Log.d(TAG, "responseText:Thanks sent to client");
                            connection.sendTCP(response);
                        }

                    }

//                    LocationRequest request = (LocationRequest)object;
//                    Log.d(TAG, "Received location requestText: " + request.getMessage());
//
//                    LocationResponse response = new LocationResponse();
//                    response.setMessage("Thanks");
//                    Log.d(TAG, "responseText:Thanks sent to client");
//                    connection.sendTCP(response);
                }


                if (object instanceof AnswerRequest) {

                    synchronized (requestPending){

                        requestPending.set(false);
                        peerChosen.set(false);

                        AnswerRequest request = (AnswerRequest)object;
                        Log.d(TAG, "Received Answer requestText: " + request.getRequest());

                        responseText = request.getResponse();

                        Log.d("sender", "Broadcasting message");
                        Intent intent = new Intent("response-received");
                        // You can also include some extra data.
                        intent.putExtra("message", responseText);
                        //intent.putExtra("request", responseText);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);


                        AnswerResponse response = new AnswerResponse();
                        response.setMessage("Thanks");
                        Log.d(TAG, "responseText:Thanks sent to client");
                        connection.sendTCP(response);

                    }


                }
            }
        });
    }
}
