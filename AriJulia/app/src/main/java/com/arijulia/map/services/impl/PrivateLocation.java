package com.arijulia.map.services.impl;

import android.location.Location;
import android.util.Log;

import com.arijulia.map.services.impl.GPSTracker;
import org.apache.commons.math3.distribution.LaplaceDistribution;

import java.util.Random;

/**
 * This class will generate a differentially private location with respect to the P2P vicinity of the user.
 */
public class PrivateLocation extends Location{

    public String type; // takes "mean" or "location" depending on type of DP generation
    public Location initialLocation;
    public Location privateLocation; // will be computed
    public Float eps; // privacy level
    public int p2pRange;
    public GPSTracker gps;

    // Class constructor using GPSTracker as input
    public PrivateLocation(GPSTracker inputGps, String inputType, Location[] locationArray, Float inputEps, int inputeRange) {
        super(inputGps.getLocation());
        gps = inputGps;
        type = inputType;
        eps = inputEps;
        initialLocation = inputGps.getLocation();
        p2pRange = inputeRange;
        if (type=="mean") {
            privateLocation = dpMean(locationArray);
        }
        else privateLocation = dpLocation();
    }

    // class constructor using location as Input
    public PrivateLocation(Location l, String inputType, Location[] locationArray, Float inputEps, int inputRange) {
        super(l);
        //gps = inputGps;
        type = inputType;
        eps = inputEps;
        initialLocation = l;
        p2pRange = inputRange;
        Log.d("p2pRange", Double.toString(p2pRange));
        if (type=="mean") {
            privateLocation = dpMean(locationArray);
        }
        else privateLocation = dpLocation();
    }

    // this method adds laplace noise to the current user's location
    public Location dpLocation() {

        double lat = this.getLatitude();
        double lon = this.getLongitude();

        // set Laplace parameters
        double beta = 1/this.eps;

        // generate laplace noise
        LaplaceDistribution laplaceLon = new LaplaceDistribution(lon,beta);
        LaplaceDistribution laplaceLat = new LaplaceDistribution(lat,beta);
        // LaplaceDistribution laplace = new LaplaceDistribution(0,beta);
        // create noise within p2p vicinity

        ///////// Wrong ////////////////
        // Using a dirty trick:
        //double max = laplace.inverseCumulativeProbability(1); // positive offset within distribution
        //Log.d("max", Double.toString(max));
        // convert to offset within p2p range in meters
        //double factor = this.p2pRange/max;
        //Log.d("factor", Double.toString(factor));
        /////////////////////////

        // 1. Longitude
        Random r = new Random();
        double prob = r.nextDouble();
        double lonDistanceInMeters = laplaceLon.inverseCumulativeProbability(prob);//*factor;
        Log.d("lonDistanceInMeters", Double.toString(lonDistanceInMeters));
        // add distance in geo coordinates (approximation works with 10 to 100m)
        double deltaLong = (90/Math.PI)*(lonDistanceInMeters/6378137)/Math.cos(lat);
        Log.d("deltaLong", Double.toString(deltaLong));
        lon = lon + deltaLong;

        // 2. Latitude
        r = new Random();
        prob = r.nextDouble();
        double latDistanceInMeters = laplaceLat.inverseCumulativeProbability(prob);//*factor;
        // add distance in geo coordinates (approximation works with 10 to 100m)
        double deltaLat = (180/Math.PI)*(latDistanceInMeters/6378137);
        Log.d("deltaLat", Double.toString(deltaLat));
        lat = lat + deltaLat;

        // update location coordinates
        Location dpLocation = this;
        dpLocation.setLatitude(lat);
        dpLocation.setLongitude(lon);
        return dpLocation;
    }

    // this method computes private mean with respect to induces P2P vicinity of other users
    // TODO: write this method
    public Location dpMean(Location[] locationArray) {
        // location of all users induces a polytop

        double latitude = this.getLatitude();
        double longitude = this.getLongitude();
        // we construct smallest circle holding this polytop
        PrivateLocation mean = this;
        mean.setLatitude(latitude);
        mean.setLongitude(longitude);
        // we compute center of the polytop as new location and call dpLocation();
        return mean.dpLocation();
    }

    // This an approximation to compute a bounding box of a Location.
    // This is quite precise as long as the distance offset is below 10-100 km.
    private double[] getBoundingBox(final double pLatitude, final double pLongitude, final int pDistanceInMeters) {

        final double[] boundingBox = new double[4];

        final double deltaLat = (180/Math.PI)*(pDistanceInMeters/6378137);
        final double deltaLong = (90/Math.PI)*(pDistanceInMeters/6378137)/Math.cos(pLatitude);

        // Alternatively use:
        //final double latRadian = Math.toRadians(pLatitude);
        //final double degLatKm = 110.574235;
        //final double degLongKm = 110.572833 * Math.cos(latRadian);
        //final double deltaLat = pDistanceInMeters / 1000.0 / degLatKm;
        //final double deltaLong = pDistanceInMeters / 1000.0 / degLongKm;

        final double minLat = pLatitude - deltaLat;
        final double minLong = pLongitude - deltaLong;
        final double maxLat = pLatitude + deltaLat;
        final double maxLong = pLongitude + deltaLong;

        boundingBox[0] = minLat;
        boundingBox[1] = minLong;
        boundingBox[2] = maxLat;
        boundingBox[3] = maxLong;

        return boundingBox;
    }
}