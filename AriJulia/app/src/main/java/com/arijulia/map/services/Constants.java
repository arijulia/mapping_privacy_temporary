package com.arijulia.map.services;

/**
 * Created by yedtoss on 2/6/16.
 */
public class Constants {
    public static final String SERVICE_TYPE = "_map_p2p._tcp";
    public static final int SERVER_TCP_PORT = 53512;
    public static final int SERVER_UDP_PORT = 48397;
    public static final int CONNECTION_TIMEOUT = 120000;
    public static final String ANDROID_STORE = "AndroidKeyStore";

    public static final String SSID_KEY = "s";
    public static final String PASSPHRASE_KEY = "p";
    public static final String ADDRESS_KEY = "a";
    public static final String TCP_PORT_KEY = "b";
    public static final String UDP_PORT_KEY = "c";

    public static final String mockSsid = "DIRECT-y7-G3";
    public static final String mockPassphrase = "bw988b7Y";
    public static final String mockAddress = "192.168.49.1";

    public static final boolean isMockedEnabled = true;
    public static final boolean isMockedPortEnabled = true;

    public static  Object object;




}
