package com.arijulia.map.services;

/**
 * Created by yedtoss on 2/8/16.
 */
public class AnswerRequest {

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    private String request;
    private String response;
}
