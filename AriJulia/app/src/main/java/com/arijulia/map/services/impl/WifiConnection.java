package com.arijulia.map.services.impl;

import android.content.Context;

import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiConfiguration;

import android.util.Log;

import java.net.InetAddress;

/**
 * Created by yedtoss on 2/7/16.
 */
public class WifiConnection {


    private WifiManager wifiManager;

    private Context context;

    private WifiConfiguration wifiConfig;

    int netId = 0;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    String address;

    private static final String TAG = "WifiConnection";

    public WifiConnection(Context context, String ssid, String password){
        this.context = context;

        this.wifiManager = (WifiManager)context.getSystemService(context.WIFI_SERVICE);

        this.wifiConfig = new WifiConfiguration();
        this.wifiConfig.SSID = String.format("\"%s\"", ssid);
        this.wifiConfig.preSharedKey = String.format("\"%s\"", password);

        this.netId = this.wifiManager.addNetwork(this.wifiConfig);
        if(wifiManager.isWifiEnabled()){
            //this.wifiManager.disconnect();
        } else {
            //wifiManager.setWifiEnabled(true);
        }
        //this.wifiManager.disconnect();
        wifiManager.setWifiEnabled(true);

        //boolean response = this.wifiManager.enableNetwork(this.netId, true);
        boolean response = this.wifiManager.enableNetwork(this.netId, false);
        if(response){
            Log.d(TAG, "Network is successfully enabled");
        } else {
            Log.d(TAG, "Could not enable the wifi access point");
        }
        response = this.wifiManager.reconnect();
        if (response) {
            Log.d(TAG, "Connection to access point successful");
        } else {
            Log.d(TAG, "Connection to access point failed");
        }




    }

    public String findAddress(){
        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
        int broadcast = (dhcpInfo.ipAddress & dhcpInfo.netmask) | ~dhcpInfo.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        try{
            address = InetAddress.getByAddress(quads).getHostAddress();
            Log.d(TAG, "Server found at : " + address);
        } catch (Exception e){
            Log.e(TAG, "e", e);
        }

        return address;
    }




    public void stop(){
        this.wifiManager.disconnect();
    }
}
