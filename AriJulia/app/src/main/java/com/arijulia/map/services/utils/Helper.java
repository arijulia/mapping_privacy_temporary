package com.arijulia.map.services.utils;

import com.arijulia.map.services.Constants;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.List;
import java.util.ArrayList;

/**
 * Created by yedtoss on 2/6/16.
 */
public class Helper {


    public static  int[] getFreePorts(int portNumber) throws IOException {

        if(Constants.isMockedPortEnabled){
            return new int[]{Constants.SERVER_TCP_PORT, Constants.SERVER_UDP_PORT};
        }
        int[] result = new int[portNumber];
        List<ServerSocket> servers = new ArrayList<ServerSocket>(portNumber);
        ServerSocket tempServer = null;

        for (int i=0; i<portNumber; i++) {
            try {
                tempServer = new ServerSocket(0);
                servers.add(tempServer);
                result[i] = tempServer.getLocalPort();
            } finally {
                for (ServerSocket server : servers) {
                    try {
                        server.close();
                    } catch (IOException e) {
                        // Continue closing servers.
                    }
                }
            }
        }
        return result;
    }


    public static int toInt(String num){
        return Integer.parseInt(num);
    }
}
