package com.arijulia.map.services.impl;

import com.arijulia.map.services.AnswerRequest;
import com.arijulia.map.services.AnswerResponse;
import com.arijulia.map.services.LocationRequest;
import com.arijulia.map.services.LocationResponse;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;

/**
 * Created by yedtoss on 2/6/16.
 */
public class SocketCommnunication {

    // This registers objects that are going to be sent over the network.
    public static void register(EndPoint endPoint){
        Kryo kryo = endPoint.getKryo();
        kryo.register(LocationRequest.class);
        kryo.register(LocationResponse.class);
        kryo.register(AnswerRequest.class);
        kryo.register(AnswerResponse.class);
        //kryo.register(String[].class);
    }


}
