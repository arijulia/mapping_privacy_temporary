package com.arijulia.map;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.arijulia.map.services.Constants;
import com.arijulia.map.services.LocationRequest;
import com.arijulia.map.services.LocationResponse;
import com.arijulia.map.services.impl.BaseCallBack;
import com.arijulia.map.services.impl.EncryptionImpl;
import com.arijulia.map.services.impl.GPSTracker;
import com.arijulia.map.services.impl.PrivateLocation;
import com.arijulia.map.services.impl.SocketClient;
import com.arijulia.map.services.impl.SocketCommnunication;
import com.arijulia.map.services.impl.SocketServer;
import com.arijulia.map.services.impl.WifiAccessPoint;

import com.arijulia.R;
import com.arijulia.map.services.impl.WifiP2pServiceDiscovery;

import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import java.io.IOException;

public class HomeActivity extends AppCompatActivity {

    // TAG for loggging
    private static final String TAG = "HomeActivity";

    private WifiAccessPoint wifiAccesspoint = null;

    private WifiP2pServiceDiscovery wifiP2pServiceDiscovery = null;

    // GPSTracker
    GPSTracker gps;
    // PrivateLocation
    PrivateLocation privateLocation = null;
    // epsilon for DP
    private final Float eps = 0.1F;
    // input range for P2P communication
    int inputRange = 300;

    int[] freePorts;

    private TextView responseTextView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Button startP2pButton = (Button) findViewById(R.id.start_p2p);
        Button stopP2pButton = (Button) findViewById(R.id.stop_p2p);

        Button startServiceDiscoveryButton = (Button) findViewById(R.id.start_service_discovery);
        Button stopServiceDiscoveryButton = (Button) findViewById(R.id.stop_service_discovery);

        Button btnGetDPLocation = (Button) findViewById(R.id.btn_Get_DPLocation);

        // Starting the Socket server
        final SocketServer server = new SocketServer(getApplicationContext());
        server.start();
        freePorts = server.getFree_ports();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("response-received"));

        Button searchButton = (Button) findViewById(R.id.search);

        responseTextView = (TextView) findViewById(R.id.text_id);

        //
        searchButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                wifiAccesspoint = new WifiAccessPoint(getApplicationContext(), freePorts);
                wifiAccesspoint.start();

                synchronized (server.getRequestPending()){
                    server.getRequestPending().set(true);
                }

            }
        });

        // What to do when the start button is clicked
        startP2pButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                if(wifiAccesspoint == null || true){

                    wifiAccesspoint = new WifiAccessPoint(getApplicationContext(), freePorts);
                    wifiAccesspoint.start();


                    final Handler handler = new Handler();

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(Constants.isMockedEnabled){
                                final SocketClient client = new SocketClient(""+freePorts[0],
                                        ""+freePorts[1]);
                                client.start(Constants.mockAddress, new BaseCallBack() {
                                    @Override
                                    public void run() {
                                        LocationRequest request = new LocationRequest();
                                        request.setMessage("My gps location is 234:345");
                                        client.sendLocationRequest(request);
                                    }
                                });
                            }
                        }
                    }, 2000);




                } else {

                }

            }

        });

        // What to do when the stop button is clicked
        stopP2pButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                if(wifiAccesspoint != null){
                    wifiAccesspoint.stop();
                }

            }

        });


        // What to do when the start discovery button is clicked
        startServiceDiscoveryButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (wifiP2pServiceDiscovery == null) {
                    wifiP2pServiceDiscovery = new WifiP2pServiceDiscovery(getApplicationContext());
                }

                wifiP2pServiceDiscovery.start();
            }


        });

        // What to do when the start discovery button is clicked
        stopServiceDiscoveryButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (wifiP2pServiceDiscovery != null) {
                    wifiP2pServiceDiscovery.stop();
                }

            }


        });

        //wifiP2pServiceDiscovery = new WifiP2pServiceDiscovery(getApplicationContext());
        //wifiP2pServiceDiscovery.start();

        // what to do when DP location button clicked
        btnGetDPLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                gps = new GPSTracker(HomeActivity.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();

                    // Show location for testing (\n is for new line)
                    Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }

                //Location testlocation = new Location("yourprovidername");
                //testlocation.setLatitude(1.2345d);
                //testlocation.setLongitude(1.2345d);

                Location l = gps.getLocation();

                privateLocation = new PrivateLocation(l, "location", null, eps, inputRange);
                //privateLocation = new PrivateLocation(testlocation, "location", null, eps, inputRange);

                double latitude = privateLocation.getLatitude();
                double longitude = privateLocation.getLongitude();
                Toast.makeText(getApplicationContext(), "Your Private Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();

            }
        });

        //EncryptionImpl en = new EncryptionImpl();
        //en.test();





    }

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }


    // Our handler for received Intents. This will be called whenever an Intent
// with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            //String req = intent.getStringExtra("request");
            Log.d("receiver", "Got message: " + message);
            //responseTextView.setText(message + " Original request is : " + req);
            responseTextView.setText(message);

            if(wifiAccesspoint != null){
                wifiAccesspoint.stop();
            }

            AlertDialog alertDialog = new AlertDialog.Builder(HomeActivity.this).create();
            alertDialog.setTitle("Connection Info");
            alertDialog.setMessage("Answer received from peer");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            //alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            alertDialog.show();


        }
    };

}
