This is for Mapping App Competition Proposal 2 "Private Map Services"

1) The source code of the application is available at https://bitbucket.org/arijulia/mapping_privacy_temporary

It is a valid Android Studio application and can be used directly for testing



2) The pre-built APK can be downloaded here: https://goo.gl/5ccJiw




For Demonstration purpose:

Have available 2 real phones with WIFI Direct enabled


a) Start the application on the first phone. Enter any text in the text field "Enter your requestText". Click on "VALIDATE".


b) Start the application on the second phone. Click on "START DISCOVERY" button. 
You will see a Popup giving you an access point and password. This is the access point created by Phone 1. You should manually connect the phone to that access point.
And then click on "OK".


c) Once the second phone is connected, it will send the response back to the first phone.


What is demonstrated:

We demonstrated the main functionality of the app which is that User A can detect neighbor users. Pick one  user B randomly. And let that user send the request on his behalf. This means that the server won't know the real user having sent the request.

For the proof-of-concept purpose, we allowed any request and sent back a mock response. Encryption is not done for this proof-of-concept.

In this proof of concept, we required User B to manually connect to the p2p access point automatically created by user A. At later stage of development, this connection can be made automatic using WifiManager.










